#define BLYNK_TEMPLATE_ID "TMPLuop3rRjY"
#define BLYNK_DEVICE_NAME "Quickstart Template"
#define BLYNK_AUTH_TOKEN "SPmb_y7MzwREHeStfHYQ_PaUxpFzbUvz"

#define BLYNK_FIRMWARE_VERSION        "0.1.0"

#define BLYNK_PRINT Serial
//#define BLYNK_DEBUG

#define APP_DEBUG

// Uncomment your board, or configure a custom board in Settings.h
//#define USE_WROVER_BOARD
//#define USE_TTGO_T7

#include "BlynkEdgent.h"

//////////////////////////////////////////////Sensor DHT22
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>
#define DHTPIN 2
#define DHTTYPE    DHT22

DHT_Unified dht(DHTPIN, DHTTYPE);

//////////////////////////////////////////////////

Adafruit_NeoPixel pixels(1, 18, NEO_GRB + NEO_KHZ800);

//////////////////////////////////////////////////

BlynkTimer timer;

// This function is called every time the Virtual Pin 0 state changes
BLYNK_WRITE(V0)
{
  // Set incoming value from pin V0 to a variable
  int value = param.asInt();
  if (value == 1)
  {
    pixels.setPixelColor(0, pixels.Color(8, 8, 8));
    pixels.show();
  }
  else
  {
    pixels.setPixelColor(0, pixels.Color(0, 0, 0));
    pixels.show();
  }
  // Update state
  Blynk.virtualWrite(V1, value);
}

// This function is called every time the device is connected to the Blynk.Cloud
BLYNK_CONNECTED()
{
  // Change Web Link Button message to "Congratulations!"
//  Blynk.setProperty(V3, "offImageUrl", "https://static-image.nyc3.cdn.digitaloceanspaces.com/general/fte/congratulations.png");
//  Blynk.setProperty(V3, "onImageUrl",  "https://static-image.nyc3.cdn.digitaloceanspaces.com/general/fte/congratulations_pressed.png");
//  Blynk.setProperty(V3, "url", "https://docs.blynk.io/en/getting-started/what-do-i-need-to-blynk/how-quickstart-device-was-made");
}

// This function sends Arduino's uptime every second to Virtual Pin 2.
void myTimerEvent()
{
  // You can send any value at any time.
  // Please don't send more that 10 values per second.
  // Get temperature event and print its value.
  sensors_event_t event;
  dht.temperature().getEvent(&event);
  if (isnan(event.temperature)) {
    // Serial.println(F("Error reading temperature!"));
  }
  else {
//    Serial.print(F("Temperature: "));
//    Serial.print(event.temperature);
//    Serial.println(F("°C"));
  Blynk.virtualWrite(V2, event.temperature);
  }
  // Get humidity event and print its value.
  dht.humidity().getEvent(&event);
  if (isnan(event.relative_humidity)) {
//    Serial.println(F("Error reading humidity!"));
  }
  else {
//    Serial.print(F("Humidity: "));
//    Serial.print(event.relative_humidity);
//    Serial.println(F("%"));
    Blynk.virtualWrite(V4, event.relative_humidity);
  }
//  Blynk.virtualWrite(V2, millis() / 1000);
//  Blynk.virtualWrite(V4, millis() / 1000);
}



//////////////////////////////////////////////////
void setup()
{
  Serial.begin(115200);
  delay(100);
  BlynkEdgent.begin();
  dht.begin();

  timer.setInterval(1000L, myTimerEvent);
  sensor_t sensor;
  dht.temperature().getSensor(&sensor);
  dht.humidity().getSensor(&sensor);
}

void loop() {
  BlynkEdgent.run();
  timer.run();
}
